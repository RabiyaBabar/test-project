class RemoveResetDigestFromEmployees < ActiveRecord::Migration[6.0]
  def change

    remove_column :employees, :reset_digest, :string
  end
end
