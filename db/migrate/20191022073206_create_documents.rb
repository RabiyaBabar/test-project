class CreateDocuments < ActiveRecord::Migration[6.0]
  def change
    create_table :documents do |t|
      t.string :documentable_type
      t.integer :documentable_id

      t.timestamps
    end
  end
end
