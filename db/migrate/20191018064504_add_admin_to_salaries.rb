class AddAdminToSalaries < ActiveRecord::Migration[6.0]
  def change
    add_column :salaries, :admin_id, :integer
  end
end
