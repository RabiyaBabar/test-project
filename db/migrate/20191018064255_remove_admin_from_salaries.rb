class RemoveAdminFromSalaries < ActiveRecord::Migration[6.0]
  def change

    remove_column :salaries, :admin_id, :string
  end
end
