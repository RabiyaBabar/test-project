class RemoveResetSentAtFromEmployees < ActiveRecord::Migration[6.0]
  def change

    remove_column :employees, :reset_sent_at, :datetime
  end
end
