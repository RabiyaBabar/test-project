class RemoveStatusFromEmployees < ActiveRecord::Migration[6.0]
  def change

    remove_column :employees, :status, :integer
  end
end
