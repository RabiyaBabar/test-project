class AddSalaryToEmployees < ActiveRecord::Migration[6.0]
  def change
    add_column :employees, :gross_salary, :integer
  end
end
