class AddColumnToEmployees < ActiveRecord::Migration[6.0]
  def change
    add_column :employees, :status, :boolean, default: true, null: false
  end
end
