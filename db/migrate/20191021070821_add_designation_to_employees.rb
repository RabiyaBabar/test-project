class AddDesignationToEmployees < ActiveRecord::Migration[6.0]
  def change
    add_column :employees, :designation_id, :integer
  end
end
