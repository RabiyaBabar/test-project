class CreateSalaries < ActiveRecord::Migration[6.0]
  def change
    create_table :salaries do |t|
      t.integer :amount_paid
      t.integer :bonus
      t.string :admin_id
      t.datetime :date_payed
      t.references :employee, null: false, foreign_key: true

      t.timestamps
    end
  end
end
