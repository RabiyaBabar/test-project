module SessionsHelper
# Logs in the given user.
  def log_in(employee)
    session[:employee_id] = employee.id
  end

  # Returns the current logged-in user (if any).
  def current_user

    if session[:employee_id]
      @current_user ||= Employee.find_by(id: session[:employee_id])
    end
  end

  def current_user?(employee)
    employee == current_user
  end
  # Returns true if the user is logged in, false otherwise.
  def logged_in?
    !current_user.nil?
  end

  def log_out
    session.delete(:employee_id)
    @current_user = nil
  end
end
