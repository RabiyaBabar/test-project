class SessionsController < ApplicationController
  def new
  end

  def create
    employee = Employee.find_by(email: params[:session][:email].downcase)
    if employee.status == true
      if employee && employee.authenticate(params[:session][:password])
        session[:employee_id] = employee.id
        redirect_to portal_path
      else
        flash.now[:danger] = 'Invalid email/password combination'
        render 'new'
      end
    else
      flash.now[:danger] = "Your Account is Deactivated"
      render 'new'
    end

  end

  def destroy
    log_out
    redirect_to root_url
  end
end
