class PasswordResetsController < ApplicationController

  before_action :get_employee,   only: [:edit, :update]
  before_action :valid_employee, only: [:edit, :update]
  before_action :check_expiration, only: [:edit, :update]
  def new
  end
def create
  @employee = Employee.find_by(email: params[:password_reset][:email].downcase)
  if @employee
    @employee.create_reset_digest
    @employee.send_password_reset_email
    flash[:info] = "Email sent with password reset instructions"
redirect_to root_url

  else
    flash[:danger] = "Email address not found"
    render 'new'

  end
end

  def update
    if params[:employee][:password].empty?                  # Case (3)
      @employee.errors.add(:password, "can't be empty")
      render 'edit'
    elsif @employee.update_attributes(employee_params)          # Case (4)
      flash[:success] = "Password has been reset."
      redirect_to root_url
    else
      render 'edit'                                     # Case (2)
    end
  end

  def edit
  end

  private
  def employee_params
    params.require(:employee).permit(:password, :password_confirmation)
  end

  def get_employee
    @employee = Employee.find_by(email: params[:email])
  end

  # Checks expiration of reset token.
  def check_expiration
    if @employee.password_reset_expired?
      flash[:danger] = "Password reset has expired."
      redirect_to new_password_reset_url
    end
  end

  # Confirms a valid user.
  def valid_employee
    unless (@employee &&
        @employee.authenticated?(:reset, params[:id]))
      redirect_to root_url
    end
  end

end
