class DesignationsController < ApplicationController
  def show
    @designation = Designation.find(params[:id])
  end

  def new
    @designation = Designation.new
  end


  def edit
    @designation = Designation.find(params[:id])
  end

  def create
    @designation = Designation.new(salary_params)
    if @designation.save

      redirect_to portal_path
    else
      render 'new'
    end
  end

  def index

    if current_user.admin
      @salaries = Salary.all
    else
      @salaries = current_user.salaries
    end

    @designation = Designation.all

  end


  def update
    @designation = Designation.find(params[:id])
    if @designation.update_attributes(designation_params)
      flash[:success] = "Designation updated"
      redirect_to portal_path
    else
      render 'edit'
    end
  end

  def destroy
    Designation.find(params[:id]).destroy
    flash[:success] = "Designation deleted"
    redirect_to portal_path
  end

  private

  def designation_params
    params.require(:designation).permit(:name)
  end
end
