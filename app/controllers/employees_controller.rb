class EmployeesController < ApplicationController
  before_action :check_login?
  def portal
    @salaries = current_user.salaries
  end

  def show
    @employee = Employee.find(params[:id])
  end

  def new
    @employee = Employee.new
    @designation = Designation.all
    @document = @employee.documents.profile_pic.first.present? ? @employee.documents.profile_pic.first : @employee.documents.profile_pic.build
  end

  def adminsignup
    @employee = Employee.new(admin: true)

  end

  def edit
    @employee = Employee.find(params[:id])
    @designation = Designation.all
  end

  def employee_edit
    @employee = current_user
    @designation = Designation.all
  end

  def create

    @employee = Employee.new(employee_params)

    if @employee.save
      if @employee.admin==true
        redirect_to alladmins_path
      else
        redirect_to employees_path
      end

    else
      render 'new'
    end
  end

  def index
    @employees = Employee.paginate(page: params[:page])
  end

  def alladmins
    @employees = Employee.paginate(page: params[:page])
  end

  def deactivate
    @employee = Employee.find(params[:employee_id])
    if current_user.admin?
      @employee.deactivate_account!
      flash[:success] = "Deactivated"
      redirect_to employees_path
    else
      redirect_to :back
    end
  end


  def activate
    @employee = Employee.find(params[:employee_id])
    if current_user.admin?
      @employee.activate_account!
      flash[:success] = "Activated"
      redirect_to employees_path
    else
      redirect_to :back
    end
  end


  def update
    @employee = Employee.find(params[:id])
    @designation = Designation.all
    @document = @employee.documents.profile_pic.first.present? ? @employee.documents.profile_pic.first : @employee.documents.profile_pic.build
    if current_user.admin
      if @employee.update(employee_params)
        update_image
        flash[:success] = "Profile updated"
        redirect_to employees_path
      else
        render 'edit'
      end
    else
      if @employee.update(employee_params)
        update_image
        flash[:success] = "Profile updated"
        redirect_to employee_path
      else
        render 'employee_edit'
      end
    end
    # @employee.documents.find(params[:employee][:documents_attributes]["0"][:id]).update(avatar: params[:employee][:documents_attributes]["0"][:avatar])
  end

  def update_image
    if(@document.persisted?)
      @document.update(avatar: params[:employee][:avatar])
    else
      @document.avatar = params[:employee][:avatar]
      @document.save
    end
  end


  def destroy
    Employee.find(params[:id]).destroy
    flash[:success] = "Employee deleted"
    redirect_to employees_path
  end

  private

  def employee_params
    params.require(:employee).permit(:name, :email, :password, :password_confirmation, :joining_date, :status, :admin, :gross_salary, :designation_id,
                                     documents_attributes: [:id, :documentable_type, :documentable_id,
                                                            :document_type, :avatar])



  end

end
