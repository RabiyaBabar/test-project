class ApplicationController < ActionController::Base
  before_action :prepare_exception_notifier


  protect_from_forgery with: :exception
  include SessionsHelper

  def check_login?
    unless logged_in?
      redirect_to login_path
      flash[:danger] =" You are not logged in"
    end
  end

  private

  def prepare_exception_notifier
    request.env["exception_notifier.exception_data"] = {
        current_user: current_user
    }
  end
end
