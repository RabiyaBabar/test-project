class SalaryController < ApplicationController

  def show
    @salary = Salary.find(params[:id])
  end

  def new
    @salary = Salary.new

    @document =  @salary.documents.salary.build
  end

  def edit
    @Salary = Salary.find(params[:id])
  end

  def create
    @salary = Salary.new(salary_params)
    if @salary.save

      redirect_to portal_path
    else
      render 'new'
    end
  end

  def index

    if current_user.admin
      # if params[:employee_id]
      #   @employee = Employee.find_by(id: params[:employee_id])
      #   @salaries = @employee.salaries
      # else
      @salaries = Salary.all
      # end

    else
      @salaries = current_user.salaries
    end


  end


  def update
    @salary = Salary.find(params[:id])
    if @salary.update_attributes(salary_params)
      flash[:success] = "Salary updated"
      redirect_to salary_path
    else
      render 'edit'
    end
  end

  def destroy
    Salary.find(params[:id]).destroy
    flash[:success] = "Salary deleted"
    redirect_to salary_index_path
  end

  private

  def salary_params
    params.require(:salary).permit(:amount_paid, :bonus, :admin_id,
                                   :date_payed, :employee_id,
                                   documents_attributes: [:id, :documentable_type, :documentable_id,
                                                                                      :document_type, :avatar])
  end
end
