class Document < ApplicationRecord
  belongs_to :documentable, polymorphic: true

  enum document_type: [:salary, :profile_pic]

  has_one_attached :avatar

end
