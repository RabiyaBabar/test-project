class Employee < ApplicationRecord
  has_many :salaries, dependent: :destroy
  has_many :processed_salaries, class_name: "Salary", foreign_key: "admin_id"
  has_many :documents, as: :documentable
  accepts_nested_attributes_for :documents, reject_if: :all_blank


  belongs_to :designation



  scope :admins, -> { where(admin: true) }
  scope :employees, -> { where(admin: false) }


  attr_accessor :reset_token
  before_save { self.email = email.downcase }
  validates :name, presence: true, length: {maximum: 50}
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence: true, length: {maximum: 255},
            format: {with: VALID_EMAIL_REGEX},
            uniqueness: {case_sensitive: false}
  validates :joining_date, presence: true
  validates :gross_salary, numericality: {only_integer: true}
  has_secure_password
  validates :password, presence: true, length: {minimum: 6}

  def image_path

    self.documents.profile_pic.first

  end



  def activate_account!
    update_attribute :status, true
  end

  def deactivate_account!
    update_attribute :status, false
  end

  # Returns the hash digest of the given string.
  def Employee.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
        BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end

  # Returns a random token.
  def Employee.new_token
    SecureRandom.urlsafe_base64
  end


  def password_reset_expired?
    reset_sent_at < 2.hours.ago
  end

  # Sets the password reset attributes.
  def create_reset_digest
    self.reset_token = Employee.new_token
    update_attribute(:reset_digest, Employee.digest(reset_token))
    update_attribute(:reset_sent_at, Time.zone.now)
  end

  # Sends password reset email.
  def send_password_reset_email
    EmployeeMailer.password_reset(self).deliver_now
  end

  def authenticated?(attribute, token)
    digest = send("#{attribute}_digest")
    return false if digest.nil?
    BCrypt::Password.new(digest).is_password?(token)
  end

end
