class Salary < ApplicationRecord
  belongs_to :employee
  belongs_to :processed_by, class_name: 'Employee', foreign_key: 'admin_id'
  has_many :documents, as: :documentable
  default_scope -> { order(amount_paid: :ASC) }
  accepts_nested_attributes_for :documents, reject_if: :all_blank


  validates :amount_paid, presence: true
  validates :bonus, presence: true
  validates :date_payed, presence: true
  validates :employee_id, presence: true


end
