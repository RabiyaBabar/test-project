Rails.application.routes.draw do


  get 'password_resets/new'
  get 'password_resets/edit'
  root "sessions#new"
  get '/signup', to: 'employees#new'
  get '/login', to: 'sessions#new'
  post '/login', to: 'sessions#create'
  delete '/logout', to: 'sessions#destroy'
  get '/portal', to: 'employees#portal'


  get '/adminsignup', to: 'employees#adminsignup'
  get '/alladmins', to: 'employees#alladmins'


  resources :password_resets, only: [:new, :create, :edit, :update]
  resources :admins
  resources :salary
  resources :designations
  resources :employees do
    collection do
      get 'employee_edit'
      get 'employee_portal'
    end

    post "/deactivate", to: "employees#deactivate"
    post "/activate", to: "employees#activate"
  end


  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
